<?php

namespace AlexTsarkov\YandexDirectGenerator;

abstract class NameFilter
{
    public static function create(string $pattern = ''): callable
    {
        if ('' === $pattern) {
            return static function (): bool {
                return true;
            };
        }

        $regex = static::compile($pattern);

        return static function (string $name) use ($regex): bool {
            return preg_match($regex, $name);
        };
    }

    public static function negate(callable $filter): callable
    {
        return static function (string $name) use ($filter): bool {
            return !$filter($name);
        };
    }

    protected static function compile(string $pattern): string
    {
        $regex = preg_replace_callback('/./', static function (array $match) use ($pattern): string {
            if ('A' <= ucfirst($match[0]) && 'Z' >= ucfirst($match[0])) {
                return $match[0];
            }

            switch ($match[0]) {
                case '*':
                    return '[A-Za-z]*';
                case '?':
                    return '[A-Za-z]';
                case '/':
                case '\\':
                    return '\\\\';
                case '{':
                    return '(?:';
                case '}':
                    return ')';
                case ',':
                    return '|';
                case '[':
                case ']':
                    return $match[0];
            }

            throw new \LogicException("Unexpected char '{$match[0]}' in pattern '{$pattern}'");
        }, $pattern);

        $regex = "/^{$regex}$/";
        if (false === @preg_match($regex, '')) {
            throw new \LogicException("Unable to compile pattern '{$pattern}'");
        }

        return $regex;
    }
}
