<?php

namespace AlexTsarkov\YandexDirectGenerator\Command;

use AlexTsarkov\YandexDirectGenerator\NameFilter;
use gossi\codegen\generator\CodeFileGenerator;
use gossi\codegen\model\PhpClass;
use gossi\codegen\model\PhpConstant;
use gossi\codegen\model\PhpInterface;
use gossi\codegen\model\PhpMethod;
use gossi\codegen\model\PhpProperty;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Filesystem\Filesystem;

class GenerateCommand extends Command
{
    protected static $defaultName = 'generate';

    protected static $defaultService = [
        'adextensions',
        'adgroups',
        'adimages',
        'ads',
        'agencyclients',
        'audiencetargets',
        'bids',
        'bidmodifiers',
        'campaigns',
        'changes',
        'clients',
        'creatives',
        'dictionaries',
        'dynamictextadtargets',
        'keywordbids',
        'keywords',
        'keywordsresearch',
        'leads',
        'retargetinglists',
        'sitelinks',
        'turbopages',
        'vcards',
    ];

    /**
     * @var string
     */
    protected $outputDirectory = 'src';

    /**
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    protected $filesystem;

    /**
     * @var string
     */
    protected $prefix = 'Yandex\\Direct';

    /**
     * @var callable
     */
    protected $filter;

    /**
     * @var \gossi\codegen\generator\CodeGenerator
     */
    protected $generator;

    protected $uriMap = [];

    protected $typeMap = [
        'int' => 'int',
        'long' => 'int',
        'decimal' => 'float',
        'string' => 'string',
    ];

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $services = $input->getArgument('service') ?: static::$defaultService;
        foreach ($services as $service) {
            $service = strtolower($service);
            if (!in_array($service, static::$defaultService, true)) {
                $output->writeln("Unknown service <info>'{$service}'</info>");
            }
            foreach ($this->parseService($output, $service) as $name => $model) {
                $filename = strtr($name, '\\', DIRECTORY_SEPARATOR) . '.php';

                if (!($this->filter)($name)) {
                    $output->writeln(
                        "Skipping file <comment>'{$filename}'</comment>",
                        OutputInterface::VERBOSITY_VERY_VERBOSE
                    );

                    continue;
                }

                $output->writeln(
                    "Writing file <info>'{$filename}'</info>",
                    OutputInterface::VERBOSITY_VERBOSE
                );

                $this->filesystem->dumpFile(
                    $input->getOption('output-dir') . DIRECTORY_SEPARATOR . $filename,
                    $this->generator->generate($model)
                );
            }
        }
    }

    protected function configure()
    {
        $this->addArgument('service', InputArgument::IS_ARRAY, 'Service name');
        $this->addOption('output-dir', 'd', InputOption::VALUE_REQUIRED, 'Outputs files to directory', $this->outputDirectory);
        $this->addOption('prefix', 'p', InputOption::VALUE_REQUIRED, 'Adds prefix to namespace', $this->prefix);
        $this->addOption('name', '', InputOption::VALUE_REQUIRED, 'Filters class names');
        $this->addOption('not-name', '', InputOption::VALUE_NONE, 'Negate filter');
        $this->setDescription('Generates classes based on WSDL');
        $this->setHelp(
            "Available service names:\n" . join('', array_map(static function (string $service): string {
                return "  <info>{$service}</info>\n";
            }, static::$defaultService))
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->filesystem = new Filesystem();
        $this->outputDirectory = $input->getOption('output-dir');

        $this->prefix = trim(strtr($input->getOption('prefix'), '/', '\\'), '\\');
        if ('' !== $this->prefix) {
            $this->prefix = "{$this->prefix}\\";
        }

        $pattern = $input->getOption('name');
        if (is_string($pattern)) {
            $this->filter = NameFilter::create($pattern);
            if (false !== $input->getOption('not-name')) {
                $this->filter = NameFilter::negate($this->filter);
            }
        } else {
            $this->filter = NameFilter::create();
        }

        $this->generator = new CodeFileGenerator([
            'generateEmptyDocblock' => false,
            'generateScalarTypeHints' => true,
            'generateReturnTypeHints' => true,
        ]);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if ($this->filesystem->exists($this->outputDirectory)) {
            $helper = $this->getHelper('question');
            $question = new Question(
                "Output directory '<info>{$this->outputDirectory}'</info> exists. You may type new directory name: ",
                $this->outputDirectory
            );
            $this->outputDirectory = $helper->ask($input, $output, $question);
        }
    }

    protected function parseService(OutputInterface $output, string $service): iterable
    {
        $wsdl = "https://api.direct.yandex.com/v5/{$service}?wsdl";
        $output->writeln(
            "Loading definition <info>'{$wsdl}'</info>",
            OutputInterface::VERBOSITY_VERBOSE
        );
        $defs = $this->loadEntity($wsdl);
        if ('definitions' !== $defs->localName) {
            $output->writeln("Unexpected tag name <info>'{$defs->localName}'</info>");

            return;
        }

        $uri = $defs->getAttribute('targetNamespace');
        $this->uriMap[$uri] = $defs->getAttribute('name');

        foreach ($defs->getElementsByTagName('types') as $types) {
            foreach ($types->getElementsByTagName('schema') as $schema) {
                yield from $this->parseTypes($output, $schema);
            }
        }

        foreach ($defs->getElementsByTagName('portType') as $portType) {
            yield from $this->parsePortType($uri, $portType);
        }
    }

    protected function loadEntity(string $url): \DOMElement
    {
        $document = new \DOMDocument();
        if (@!$document->load($url, LIBXML_COMPACT)) {
            throw new \RuntimeException("Unable to load external entity '{$url}'");
        }
        $document->normalizeDocument();

        return $document->documentElement;
    }

    protected function importTypes(OutputInterface $output, \DOMElement $import): iterable
    {
        $uri = $import->getAttribute('namespace');
        if (isset($this->uriMap[$uri])) {
            $output->writeln(
                "Skipping schema <comment>'{$uri}'</comment>",
                OutputInterface::VERBOSITY_VERY_VERBOSE
            );

            return;
        }

        $schemaLocation = $import->getAttribute('schemaLocation');
        $output->writeln(
            "Loading schema <info>'{$uri}'</info> from <info>'{$schemaLocation}'</info>",
            OutputInterface::VERBOSITY_VERBOSE
        );
        $schema = $this->loadEntity($schemaLocation);
        $this->uriMap[$uri] = '';

        yield from $this->parseTypes($output, $schema);
    }

    protected function parseTypes(OutputInterface $output, \DOMElement $schema): iterable
    {
        $uri = $schema->getAttribute('targetNamespace');
        foreach ($schema->childNodes as $type) {
            switch ($type->localName) {
                case 'import':
                    yield from $this->importTypes($output, $type);

                    break;
                case 'simpleType':
                    yield from $this->parseSimpleType($uri, $type);

                    break;
                case 'complexType':
                case 'element':
                    yield from $this->parseComplexType($uri, $type);

                    break;
            }
        }
    }

    protected function parseSimpleType(string $uri, \DOMElement $type): iterable
    {
        $name = trim("{$this->uriMap[$uri]}\\{$type->getAttribute('name')}", '\\');
        $qname = "{$this->prefix}{$name}";

        $model = PhpInterface::create($qname);
        foreach ($type->getElementsByTagName('enumeration') as $enum) {
            $value = $enum->getAttribute('value');
            $model->setConstant(PhpConstant::create(strtoupper($value), $value));
        }

        $this->typeMap[$qname] = 'string';

        yield $name => $model;
    }

    protected function parseComplexType(string $uri, \DOMElement $type): iterable
    {
        $name = trim("{$this->uriMap[$uri]}\\{$type->getAttribute('name')}", '\\');
        $qname = "{$this->prefix}{$name}";

        $model = PhpClass::create($qname);

        $ext = $type->getElementsByTagName('extension')->item(0);
        if ($ext) {
            $model->setParentClassName("{$this->lookupName($ext, 'base')}");
        }

        foreach ($type->getElementsByTagName('element') as $elem) {
            $property = PhpProperty::create(lcfirst($elem->getAttribute('name')))
                ->setVisibility('public')
            ;

            $type = $this->lookupName($elem, 'type');
            if ('unbounded' === $elem->getAttribute('maxOccurs')) {
                $type = "{$type}[]";
                $property->setExpression('[]');
            } elseif ('0' === $elem->getAttribute('minOccurs')) {
                $type = "?{$type}";
            }
            $property->setType($type);

            $model->setProperty($property);
        }

        yield $name => $model;
    }

    protected function parsePortType(string $uri, \DOMElement $portType): iterable
    {
        $name = "{$this->uriMap[$uri]}\\{$this->uriMap[$uri]}Service";
        $qname = "{$this->prefix}{$name}";

        $model = PhpClass::create($qname)
            ->setParentClassName("{$this->prefix}Service")
            ->setConstant(PhpConstant::create('ENDPOINT', basename($uri)))
        ;

        foreach ($portType->getElementsByTagName('operation') as $op) {
            $method = PhpMethod::create(lcfirst($op->getAttribute('name')))
                ->setVisibility('public')
                ->addSimpleParameter('request')
            ;

            $model->setMethod($method);
        }

        yield $name => $model;
    }

    protected function lookupName(\DOMElement $node, string $attr): string
    {
        $parts = explode(':', $node->getAttribute($attr), 2);
        if (1 === count($parts)) {
            array_unshift($parts, '');
        }
        $parts[0] = $node->lookupNamespaceUri($parts[0]);
        [$uri, $type] = $parts;

        if (!isset($this->uriMap[$uri])) {
            return $this->typeMap[$type] ?? $type;
        }

        $name = trim("{$this->uriMap[$uri]}\\{$type}", '\\');
        $qname = "{$this->prefix}{$name}";

        return $this->typeMap[$qname] ?? $qname;
    }
}
