<?php

(function (string ...$try_files) {
    foreach ($try_files as $file) {
        if (is_file($file)) {
            return require $file;
        }
    }
    die('Unable to locate autoload at ' . join(', ', $try_files));
})(
    dirname(__DIR__) . '/vendor/autoload.php',
    dirname(__DIR__, 2) . '/autoload.php'
);

use AlexTsarkov\YandexDirectGenerator\Command\GenerateCommand;
use Symfony\Component\Console\Application;

$application = new Application('Yandex.Direct Generator');
$command = $application->add(new GenerateCommand());
$application->setDefaultCommand($command->getName(), true);
$application->run();
