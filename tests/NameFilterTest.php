<?php

namespace AlexTsarkov\YandexDirectGenerator;

use PHPUnit\Framework\TestCase;

/**
 * @covers \AlexTsarkov\YandexDirectGenerator\NameFilter
 *
 * @internal
 */
final class NameFilterTest extends TestCase
{
    /**
     * @testWith [""]
     *           ["A"]
     *           ["A\\Z"]
     */
    public function testAny(string $name)
    {
        $filter = NameFilter::create('');
        $this->assertIsCallable($filter);
        $this->assertTrue($filter($name));
    }

    /**
     * @testWith ["", false]
     *           ["A", true]
     *           ["Z", true]
     *           ["\\", false]
     *           ["Ab", false]
     */
    public function testOneLetter(string $name, bool $expect)
    {
        $filter = NameFilter::create('?');
        $this->assertIsCallable($filter);
        $this->assertSame($expect, $filter($name));
    }

    /**
     * @testWith ["", true]
     *           ["A", true]
     *           ["Z", true]
     *           ["Ab", true]
     *           ["\\", false]
     *           ["A\\", false]
     */
    public function testZeroOrMoreLetters(string $name, bool $expect)
    {
        $filter = NameFilter::create('*');
        $this->assertIsCallable($filter);
        $this->assertSame($expect, $filter($name));
    }

    /**
     * @testWith ["", false]
     *           ["A", true]
     *           ["a", true]
     *           ["Z", false]
     *           ["\\", false]
     *           ["AA", false]
     *           ["aa", false]
     */
    public function testRange(string $name, bool $expect)
    {
        $filter = NameFilter::create('[Aa]');
        $this->assertIsCallable($filter);
        $this->assertSame($expect, $filter($name));
    }

    /**
     * @testWith ["", false]
     *           ["A", false]
     *           ["a", false]
     *           ["Z", false]
     *           ["z", false]
     *           ["\\", false]
     *           ["AZ", true]
     *           ["az", true]
     *           ["AZ\\az", false]
     *           ["AZaz", false]
     */
    public function testAlteration(string $name, bool $expect)
    {
        $filter = NameFilter::create('{AZ,az}');
        $this->assertIsCallable($filter);
        $this->assertSame($expect, $filter($name));
    }

    /**
     * @testWith ["0"]
     *           ["-"]
     *           [":"]
     *           [";"]
     *           ["#"]
     *           ["~"]
     *           ["%"]
     *           ["$"]
     *           ["()"]
     *           ["["]
     *           ["[]"]
     *           ["{"]
     *           ["{{}"]
     */
    public function testIncorrect(string $pattern)
    {
        $this->expectException(\LogicException::class);
        NameFilter::create($pattern);
    }

    /**
     * @testWith [""]
     *           ["A"]
     *           ["A\\Z"]
     */
    public function testNegated(string $name)
    {
        $filter = NameFilter::negate(NameFilter::create());
        $this->assertIsCallable($filter);
        $this->assertFalse($filter($name));
    }
}
